ifneq (,)
  $(error This Makefile requires GNU Make. )
endif

# Make configuration
include Make.config
include Phony.config

# Load default environment variables
-include .env

# Default make ENV is dev,. use make [target] ENV=prod for production
ENV ?= dev

ifeq ($(filter $(ENV),dev prod),)
$(error The ENV variable is invalid. must be one of <prod|dev> )
endif

# TODO: Add debian support
# TODO: Alpine is smaller, but debian is much faster to build and may be therefore be preferred for development.
ifeq ($(PREFERRED_DISTRO),debian)
$(error Support for debian has not been added yet. Work in progress. )
endif

ifeq ($(filter $(PREFERRED_DISTRO),alpine debian),)
$(error The PREFERRED_DISTRO variable is invalid. must be one of <alpine|debian> )
endif

ifeq ($(wildcard .developer.env),)
$(error The .developer.env file does not exist. Copy .developer.env.dist to .developer.env configure the required settings)
endif

-include .developer.env

ifeq ($(DOCKER_BUILDKIT_ENABLED),true)
	DOCKERFILE := $(PREFERRED_DISTRO)-buildkit.dockerfile
else
	DOCKERFILE := $(PREFERRED_DISTRO).dockerfile
endif

ifeq ($(COMPOSER2_ENABLED),true)
	COMPOSER_IMAGE_VERSION := $(COMPOSER2_IMAGE_VERSION)
else
	COMPOSER_IMAGE_VERSION := $(COMPOSER1_IMAGE_VERSION)
endif

ifeq ("$(wildcard $(DOCKERFILE))","")
	$(error The file dockerfile ./${DOCKERFILE} does not exist! Check your settings or create the dockerfile.)
endif

PROJECT_PREFIX := $(shell echo $(strip ${PROJECT}) | tr -s -c [:alnum:] -)
PREFIX 		   := $(PROJECT_PREFIX)$(ENV)
INTERNAL_NETWORK_NAME := $(PROJECT)_internal_net
HOSTS_CONFIG ?= $(HOSTSCONFIG)
MYSQL_VOLUME ?= $(PREFIX)_mysql_volume
APP_VOLUME ?= $(PREFIX)_app_volume

COMPOSE_FILES_PATH := -f docker-compose.yaml

#
# OS specific (networks, volumes)
#

ifeq ($(OS_NAME),osx)
	# OSX CONFIG
	ifeq ($(OSX_CATALINA_OR_GREATER),true)
		OS_COMPOSE_FILE :=  -f ./compose/os/osx-catalina/${ENV}.yaml
	else
		OS_COMPOSE_FILE :=  -f ./compose/os/osx/${ENV}.yaml
	endif
else
	# Linux / Other OS
	ifneq ("$(wildcard compose/os/$(OS_NAME)/$(ENV).yaml)","")
		OS_COMPOSE_FILE :=  -f ./compose/os/${OS_NAME}/${ENV}.yaml
	else
		$(error The environment configuration is incomplete. The file ./compose/os/${OS_NAME}/${ENV}.yaml was not found!)
	endif
endif

#
# MYSQL
#

ifeq ($(MYSQL_ENABLED),true)
    ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/mysql/$(ENV).yaml)","")
       COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/mysql/$(ENV).yaml
    else
       COMPOSE_FILES_PATH +=  -f ./compose/lib/mysql/$(ENV).yaml
    endif
    # PHPMyAdmin is only available when MySQL is enabled. Enabling PHPMyAdmin without MySQL makes no sense and will fail
    # because it will not be possible to connect to the mysql container.
    ifeq ($(PMA_ENABLED),true)
    	# Phpmyadmin is only available in dev
    	ifeq ($(ENV),dev)
			ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/mysql/phpmyadmin/$(ENV).yaml)","")
			   COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/mysql/phpmyadmin/$(ENV).yaml
			else
			   COMPOSE_FILES_PATH +=  -f ./compose/lib/mysql/phpmyadmin/$(ENV).yaml
			endif
        endif
    endif
endif

#
# Mailhog (SMTP server for development)
#

ifeq ($(MAILHOG_ENABLED),true)
	ifeq ($(ENV),dev)
		ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/mailhog/$(ENV).yaml)","")
		   COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/mailhog/$(ENV).yaml
		else
		   COMPOSE_FILES_PATH +=  -f ./compose/lib/mailhog/$(ENV).yaml
		endif
	endif
endif

#
# Redis
#

ifeq ($(REDIS_ENABLED),true)
	ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/redis/$(ENV).yaml)","")
		COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/redis/$(ENV).yaml
	else
		COMPOSE_FILES_PATH +=  -f ./compose/lib/redis/$(ENV).yaml
	endif
endif

#
# RabbitMQ
#

ifeq ($(RABBITMQ_ENABLED),true)
    ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/rabbitmq/$(ENV).yaml)","")
       COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/rabbitmq/$(ENV).yaml
    else
       COMPOSE_FILES_PATH +=  -f ./compose/lib/rabbitmq/$(ENV).yaml
    endif
endif

#
# Portainer (Docker Management UI)
#

ifeq ($(PORTAINER_ENABLED),true)
	HOSTS_CONFIG += ,$(PORTAINER_VIRTUAL_HOST)
    ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/portainer/$(ENV).yaml)","")
       COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/portainer/$(ENV).yaml
    else
       COMPOSE_FILES_PATH +=  -f ./compose/lib/portainer/$(ENV).yaml
    endif
endif

#
# Solr
#

ifeq ($(SOLR_ENABLED),true)
    ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/lib/solr/$(ENV).yaml)","")
       COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/lib/solr/$(ENV).yaml
    else
       COMPOSE_FILES_PATH +=  -f ./compose/lib/solr/$(ENV).yaml
    endif
endif

# Try to load website and environment specific configuration
# For example:   compose/websites/topgeschenken.nl/prod.yaml
# Do not take overwriting the defaults this way too lightly. Only do this when needed!
# The less configuration the easier it will be to use and maintain.
ifneq ("$(wildcard compose/websites/$(SERVER_NAME)/$(ENV).yaml)","")
   COMPOSE_FILES_PATH +=  -f ./compose/websites/$(SERVER_NAME)/$(ENV).yaml
else
   COMPOSE_FILES_PATH +=  -f ./compose/$(ENV).yaml
endif

ifeq ($(ENV),prod)
   COMPOSER_ARGS := --no-dev --classmap-authoritative --prefer-dist
else
   COMPOSER_ARGS := --dev
endif

# The host string is a comma separated string containing the virtual hosts of the application.
# Optionally, additional hosts may added. The virtual host of portainer may be added for example.
WEBSHOP_HOST_STR := $(shell echo $(HOSTS_CONFIG) | tr ',' ' ')
HOSTNAMES = $(words $(WEBSHOP_HOST_STR))

# The CMD prefix provides a number of generic arguments that we want to make generally available.
CMD_PREFIX := DOCKERFILE=$(DOCKERFILE) COMPOSER_IMAGE_VERSION=$(COMPOSER_IMAGE_VERSION) DOCKER_UID=$(DOCKER_UID) DOCKER_GID=$(DOCKER_GID) PROJECT=$(PROJECT) PREFIX=$(PREFIX) INTERNAL_NETWORK_NAME=$(INTERNAL_NETWORK_NAME) SERVER_NAME=$(SERVER_NAME) APP_VERSION=$(APP_VERSION) APP_ENV=$(ENV)  COMPOSE_DOCKER_CLI_BUILD=1 APP_VOLUME=$(APP_VOLUME) MYSQL_VOLUME=$(MYSQL_VOLUME) DOCKER_BUILD_COMMIT=$(DOCKER_BUILD_COMMIT) APP_GIT_COMMIT=$(APP_GIT_COMMIT)

# --------------------------

.PHONY: $(MAKE_PHONY_TARGETS)

clean-install: stop rm volumes-rm network-remove volumes-create networks-create build up solr-create-collections
clean-install-no-cache: stop rm volumes-rm network-remove volumes-create networks-create build-no-cache up solr-create-collections

prepare:
	@make ssh-key-gen
	@make ssh-key-add

clean-install:
	make stop
	make rm
	make volumes-rm
	make volumes-rm

install:
	@make prepare
	@make pre-install
	@make project-repository
	@make build
	@make up
	@make post-install


post-clean-install:
	make app-reset

project-repository: ## Clone website repository
	@$(PWD)/workbench/bin/tasks/git-repository-webshop.sh -n

build:	  ## Build The Image
	@make pre-build
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(OS_COMPOSE_FILE)  build
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) build
	@make post-build

build-no-cache:	## Build The Image without cache (will take significantly longer but may avoid/solve dependency related problems)
	@make pre-build
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) build --no-cache
	@make post-build

up:	 ## Start containers (build if necessary)
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(OS_COMPOSE_FILE) --compatibility up --build -d
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) --compatibility up --build -d
	@- make host-manager

resume:	 ## Resume containers
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(OS_COMPOSE_FILE) --compatibility up --build -d
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) --compatibility up -d


prune: # Remove all stopped containers, unused networks, dangling images and caches related to this project
	$(DOCKER_BIN) system prune --volumes --filter name=${PROJECT}

down:	  ## Down service and do clean up
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) down

start:	  ## Start containers
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) start

stop:	  ## Stop containers
	$(DOCKER_BIN) stop `$(DOCKER_BIN) ps --filter name=$(PROJECT) -q`

ps: ## Lists project containers
	$(DOCKER_BIN) container ls --all --filter=name=$(PREFIX)

logs:	  ## Tail container logs
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) logs --follow --tail=1000

images:	  ## Show project images
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) images

networks: ## List networks
	@ echo "Listing networks..."
	$(DOCKER_BIN) network ls --filter name=$(PROJECT)

volumes: ## Lists pproject volumes
	@ echo "Listing volumes..."
	$(DOCKER_BIN) volume ls --filter name=${PROJECT}

deploy:	  ## Deploy Prod Image (alias for `make up ENV=prod`)
	@make up ENV=prod

restart:   ## Restart containers
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) restart

stats: ## Dump stats
	@$(DOCKER_BIN) stats --all --format "table {{.Container}}\t{{.CPUPerc}}\t{{.MemUsage}}"

rm:	  ## Remove containers
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) rm -f

#
# Application commands
#

app-chown: ## Set file permissions
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'chown www-data:www-data /var/www/app/ -R'

warmup: ## Cache warmup
	@echo "Warming up symfony cache... This may take while..."
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=$(ENV) /var/www/app/bin/console cache:warmup'
	@echo "Ready!"

cache-clear: ## Clear application cache
	@echo "Clear symfony cache. This may take while..."
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=$(ENV) /var/www/app/bin/console cache:clear'
	@echo "Ready!"

app-reset: ## Run symfony reset script
	# Note: For some reason, the reset command requires root privileges to update some mysql settings.
	# As a work around provide environment variables to use the root user instead, without making the application
	# aware of it. Warning: If you do NOT use the root user the command will silently fail. I had to find that out the
	# hard way.
	@echo "Execute reset command in app container..."
	@- make solr-create-collections
	@${PWD}/workbench/bin/tasks/app-reset-cmd.sh "$(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH)" "APP_ENV=dev DB_USER=root DB_PASS=$(MYSQL_ROOT_PASSWORD) DATABASE_URL=mysql://root:$(MYSQL_ROOT_PASSWORD)@mysql:${MYSQL_PORT}/${MYSQL_DATABASE}  /var/www/app/symfony reset -vvv" " -e MYSQL_ROOT_PASSWORD=$(MYSQL_ROOT_PASSWORD)"

app-solr-rebuild: ## Rebuild Solr Indexes
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=dev /var/www/app/bin/console solr:index:clear -vvv'
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=dev /var/www/app/bin/console solr:index:populate  -vvv'

app-redis-flush: ## Execute application `redis-flush` command.
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=dev /var/www/app/bin/console redis:flushall -vvv'

app-composer-scripts: ## Run composer scripts
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=dev composer run-script --no-interaction auto-scripts -d /var/www/app'

app-composer-install: ## Run composer install (always dev, dependencies are copied from an intermediate container and than packaged in production image, composer is notinstalled in the production "app" image.
	$(CMD_PREFIX) $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c "rm /var/www/app/vendor/* -R && APP_ENV=$(ENV) COMPOSER_MEMORY_LIMIT=-1 composer install $(COMPOSER_ARGS) --no-interaction -d /var/www/app"

app-composer-update: ## Run composer update (always dev, dependencies are copied from an intermediate container and than packaged in production image, composer is notinstalled in the production "app" image.
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c "APP_ENV=$(ENV) COMPOSER_MEMORY_LIMIT=-1 composer update $(COMPOSER_ARGS) --no-interaction --lock -d  /var/www/app"

assets-install: ## Install assets (APP_ENV=dev)
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c "APP_ENV=$(ENV) php  /var/www/app/bin/console assets:install"

assets:  ## Combined procedure to Copy Symfony assets, install dependencies and trigger the webpack build.
	@make assets-install
	@make ssh-key-add
	@make yarn
	@make webpack

webpack: ## Build frontend assets
	$(DOCKER_BIN) run --rm -it \
		-v ${PWD}/workspace/webshop:/app \
		node bash -ci " \
			cd /app && \
			mkdir -p /app/public/build/app /app/public/build/admin && \
			yarn build-$(ENV) \
		"

yarn: ## Install frontend packages
	$(DOCKER_BIN) run --rm -it \
		-v ${PWD}/workspace/webshop:/app \
		-v ${PWD}/workbench/docker/conf/ssh/ssh_config:/ssh_config \
		-v ${SSH_AGENT_SOCK}:/ssh_agent \
		-e SSH_AUTH_SOCK=/ssh_agent \
		-e UID=${DOCKER_UID} \
		-e GID=${DOCKER_GID} \
		node bash -ci " \
			mkdir -p /root/.npm /app/node_modules && \
			mkdir -m 700 /root/.ssh && \
			touch -m 600 /root/.ssh/config && \
			touch -m 600 /root/.ssh/known_hosts && \
			chmod 600 /root/.ssh/* && \
			chown root:root /ssh_agent /root/.ssh -R && \
			ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
			ssh-add -l && \
			cat /ssh_config > /root/.ssh/config && \
			cd /app && \
			yarn --$(ENV) && \
			yarn preinstall && \
			chown $(DOCKER_UID):$(DOCKER_GID) /app/node_modules/ -R \
		"

#
# Utility targets
#

ssh-key-add: ## Add SSH key to SSH agent
	@if test -z "$(DEVELOPER_SSH_RSA_ID)";  then \
		echo "DEVELOPER_SSH_RSA_ID environment variable is empty!"; \
		exit 64; \
	fi
	@if [ ! -f "$(HOME)/.ssh/$(DEVELOPER_SSH_RSA_ID)" ];  then \
		echo "The SSH key $(HOME)/.ssh/$(DEVELOPER_SSH_RSA_ID) does not exist."; \
		exit 64; \
	fi
	@if [ "osx" = "$(OS_NAME)" ];  then \
		echo "Adding SSH key on OSX..."; \
		eval `ssh-agent -s` && ssh-add $(HOME)/.ssh/$(DEVELOPER_SSH_RSA_ID); \
		exit 0; \
	else \
		echo "Adding SSH key on Linux..."; \
		eval `ssh-agent -s` && ssh-add $(HOME)/.ssh/$(DEVELOPER_SSH_RSA_ID); \
		export SSH_AGENT_SOCK="${SSH_AUTH_SOCK}"; \
		exit 0; \
	fi

ssh-sock:
	@echo $(SSH_AGENT_SOCK)

ssh-key-gen: ## Generate SSH key
	@echo "Generate SSH key..."
	workbench/bin/tasks/ssh-key-gen.sh -n

app-logs: ## Tail application log
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'APP_ENV=dev tail /var/www/app/var/log/*.log -f'

osx-nfs-setup: ## Create NFS mount on MacOS
	$(PWD)/bin/setup-nfs-osx.sh

gen-x509-certs: ## Generate x509 key chain (Example: make gen-ssl-cert PUBLIC_CN=dev.topgeschenken.nl
	@if test -z "$(PUBLIC_CN)";  then \
		echo "PUBLIC_CN argument is missing!"; \
		exit 64; \
	fi
	mkdir -p ${PWD}/workbench/docker/development/ssl/${PUBLIC_CN}
	sudo rm ${PWD}/workbench/docker/development/ssl/${PUBLIC_CN}/* -f
	$(DOCKER_BIN) run --rm \
    	-v ${PWD}/workbench/docker/development/ssl/${PUBLIC_CN}:/etc/ssl/certs \
    	-e COUNTY="${COMPANY_COUNTRY_CODE}" \
    	-e STATE="${COMPANY_STATE}" \
    	-e LOCATION="${COMPANY_CITY}" \
    	-e ORGANISATION="${COMPANY_NAME}" \
    	-e ISSUER_CN="${COMPANY_x509_ISSUER_CN}" \
    	-e PUBLIC_CN="${PUBLIC_CN}" \
    	-e ISSUER_NAME="${COMPANY_x509_ISSUER_NAME}" \
    	-e KEYSTORE_PASS="t0pG3sCh#nkenD3V310P3R" \
    	-e RSA_KEY_NUMBITS="${COMPANY_x509_ISSUER_NUMBITS}" \
    	-e DAYS="${COMPANY_x509_ISSUER_DAYS}" \
    	-e PUBLIC_CN="${PUBLIC_CN}" \
    	pgarrett/openssl-alpine \
    	&& sudo chown $(DOCKER_UID):$(DOCKER_GID) $(PWD)/workbench/docker/development/ssl -R \
    	&& sudo chmod ug+s $(PWD)/workbench/docker/development/ssl -R


dump-compose-config:	 ## Dump final docker-compose configuration
	@ echo "Start containers"
	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) config

dump-vars: ## Dump variables to see which settings are used for the current environment (dev|prod)
	@ echo "\nDumping variables...\n"
	@ echo "CMD_PREFIX = $(CMD_PREFIX)"
	@ echo "COMPOSE_FILES_PATH = $(COMPOSE_FILES_PATH)"
	@ echo "APP_VERSION = $(APP_VERSION)"
	@ echo "PREFIX = $(PREFIX)"
	@ echo "OS_NAME = $(OS_NAME)"
	@ echo "OSX_CATALINA_OR_GREATER = $(OSX_CATALINA_OR_GREATER)"
	@ echo "ENV = $(ENV)"
	@ echo "PREFIX = $(PREFIX)"
	@ echo "\nMONGODB SETTINGS:"
	@ echo "MONGO_IMAGE_VERSION=$(MONGO_IMAGE_VERSION)"
	@ echo "MONGO_ENABLED=$(MONGO_ENABLED)"
	@ echo "MONGO_USER=$(MONGO_USER)"
	@ echo "MONGO_PASSWORD=$(MONGO_PASSWORD)"
	@ echo "MONGO_PUBLIC_PORT=$(MONGO_PUBLIC_PORT)"
	@ echo "MONGO_PORT=$(MONGO_PORT)"
	@ echo "\nMYSQL SETTINGS:"
	@ echo "MYSQL_IMAGE_VERSION=$(MYSQL_IMAGE_VERSION)"
	@ echo "MYSQL_ENABLED=$(MYSQL_ENABLED)"
	@ echo "MYSQL_PORT=$(MYSQL_PORT)"
	@ echo "MYSQL_PUBLIC_PORT=$(MYSQL_PUBLIC_PORT)"
	@ echo "MYSQL_DATABASE=$(MYSQL_DATABASE)"
	@ echo "MYSQL_USER=$(MYSQL_USER)"
	@ echo "MYSQL_PASSWORD=$(MYSQL_PASSWORD)"
	@ echo "MYSQL_RANDOM_ROOT_PASSWORD=$(MYSQL_RANDOM_ROOT_PASSWORD)"
	@ echo "MYSQL_VERSION=$(MYSQL_VERSION)"
	@ echo "\nPHPMYADMIN SETTINGS:"
	@ echo "PMA_ENABLED=$(PMA_ENABLED)"
	@ echo "PMA_PORT=$(PMA_PORT)"
	@ echo "PMA_UI_PORT=$(PMA_UI_PORT)"
	@ echo "\n\nTo change any of these settings modify .env or the environment specific configuration in .env.$($ENV)"

virtual-hosts: ## Configure or update virtual hosts
	@if [ ${HOSTNAMES} -gt 1 ]; then \
	  for webshop_host in $(WEBSHOP_HOST_STR) ; \
		do \
		  echo $$webshop_host; \
	      make gen-x509-certs PUBLIC_CN=$$webshop_host; \
	      workbench/bin/utils/virtual-host.sh add $$webshop_host; \
		done \
	  else \
		echo $(HOSTNAMES); \
		make gen-x509-certs PUBLIC_CN=$HOSTNAMES; \
		workbench/bin/utils/virtual-host.sh add $$webshop_host; \
	fi



cleanup: ## Clean up host system (Remove hosts, delete SSL certs etc)
	#${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) down -v --rmi all --remove-orphans
	rm workbench/docker/development/ssl/* -R
	@if [ ${HOSTNAMES} -gt 1 ]; then \
	  for webshop_host in $(WEBSHOP_HOST_STR) ; \
		do \
		  echo $$webshop_host; \
	      workbench/bin/utils/virtual-host.sh remove $$webshop_host; \
		done \
	  else \
		echo $(HOSTNAMES); \
		workbench/bin/utils/virtual-host.sh remove $$webshop_host; \
	fi

host-manager: ## Starts proxy to automatically discover and forward requests to the corresponding virtual hosts
	@- $(DOCKER_BIN) stop ${PREFIX}-router > /dev/null
	@- $(DOCKER_BIN) rm ${PREFIX}-router > /dev/null
	$(DOCKER_BIN) run -d --network $(INTERNAL_NETWORK_NAME) --name ${PREFIX}-router -p 80:80 -p 443:443 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy

solr-create-collections: ## Create solr configuration
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec solr /bin/sh -c '/opt/solr/bin/solr create -n data_driven_schema_configs -c order_collection'
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec solr /bin/sh -c '/opt/solr/bin/solr create -n data_driven_schema_configs -c order'
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec solr /bin/sh -c '/opt/solr/bin/solr create -n data_driven_schema_configs -c product_translation'
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec solr /bin/sh -c '/opt/solr/bin/solr create -n data_driven_schema_configs -c voucher'
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec solr /bin/sh -c '/opt/solr/bin/solr create -n data_driven_schema_configs -c payment'

ssh-solr: ## Open shell to solr container
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec solr /bin/sh

ssh-redis: ## Open shell to redis container
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec redis /bin/sh

ssh-mysql: ## Open shell to mysql container
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec mysql /bin/sh

ssh-php:   ## Open shell to PHP container
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh

ssh-nginx: ## Open shell to nginx container
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec web /bin/sh

redis-cli: ## Open shell to Redis CLI
	@if [ "${REDIS_ENABLED}" != "true" ];  then \
		echo "Redis is not containerized. To containerize Redis, set REDIS_ENABLED to 'true' and rebuild the environment."; \
		exit 64; \
	fi
	@${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec redis /bin/sh -c 'redis-cli'


lint: lint-yaml

lint-yaml:
	@ echo "Run yamllint..."
	$(DOCKER_BIN) run --rm $$(tty -s && echo "-it" || echo) -v $(PWD):/data cytopia/yamllint:latest .
	@ echo "Yaml lint complete...\n"

help:  ## Show this help.
	@echo "Make Application Docker Images and Containers using Docker-Compose files in 'docker' Dir."
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m ENV=<prod|dev> (default: dev) SERVER_NAME=<[a-z.-]+> (default: dev.topgeschenken.nl)\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-12s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

test:
	env

#
# PRIVATE TARGETS
#
# These targets are not intended to be used directly, also some targets may not be used right now.
# TODO What targets can be removed?
#

network-create: # Create networks
	- $(DOCKER_BIN) network create $(INTERNAL_NETWORK_NAME) -d $(DOCKER_NETWORK_DRIVER) --attachable \
 		--label="nl.topgeschenken.webshop.vcs-ref=$(APP_GIT_COMMIT)" \
 		--label="nl.topgeschenken.webshop.version=$(APP_VERSION)" \
 		--label="nl.topgeschenken.webshop.env=$(ENV)" \
 		--label="nl.topgeschenken.webshop.project_label=$(PROJECT)" \
 		--label="nl.topgeschenken.webshop.build_prefix=$(PREFIX)" \
 		--label="org.label-schema.vendor=$(COMPANY_NAME)" \
 		--label="org.label-schema.build-date=$(DOCKER_BUILD_DATE)" \
 		--label="org.label-schema.schema-version=$(LABEL_SCHEMA_SPEC_VERSION)" \
 		--label="org.label-schema.version=$(DOCKER_BUILD_VERSION)" \
 		--label="org.label-schema.vcs-url=$(DOCKER_BUILD_REPOSITORY_URL)" \
 		--label="org.label-schema.vcs-ref=$(DOCKER_BUILD_COMMIT)"

network-remove: # Remove networks
	@ echo "Remove networks..."
	@- $(DOCKER_BIN) network rm $$($(DOCKER_BIN) network ls --filter name=$(PROJECT) -q)

volumes-create: # Create project volumes
	@ echo "Create volumes..."
	- $(DOCKER_BIN) volume create ${MYSQL_VOLUME} --label nl.topgeschenken.webshop.project.build_prefix=${PREFIX} --label=nl.topgeschenken.webshop.project_label=${PROJECT} --label=nl.topgeschenken.webshop.version=${APP_VERSION}
	- $(DOCKER_BIN) volume create ${APP_VOLUME}  --label nl.topgeschenken.webshop.project.build_prefix=${PREFIX} --label=nl.topgeschenken.webshop.project_label=${PROJECT} --label=nl.topgeschenken.webshop.version=${APP_VERSION}

volumes-rm: # Remove volumes (shared volumes and volumes for other webshops are not removed)
	@ echo "Remove webshop volumes..."
	$(DOCKER_BIN) volume rm $$($(DOCKER_BIN) volume ls --filter name=${PREFIX} -q)

volumes-rm-all: # Remove ALL volumes from all sites in THIS project (including shared volumes)
	@ echo "Remove ALL volumes in THIS project..."
	$(DOCKER_BIN) volume rm $$($(DOCKER_BIN) volume ls --filter label=${PROJECT} -q)

volumes-all: # List ALL volumes from all sites in THIS project.
	@ echo "Listing volumes..."
	$(DOCKER_BIN) volume ls --filter label=${PROJECT}



pre-install: # This is a private target. Add functionality here that must run BEFORE EVERY installation.
	@-make virtual-hosts

post-install: # This is a private target. Add functionality here that must run AFTER EVERY installation.
	@- make solr-create-collections
	@make host-manager
	#@make warmup

pre-build: # This is a private target. Add functionality here that must run BEFORE EVERY build.
	@- make network-remove >2 /dev/null
	@make network-create

post-build: # This is a private target. Add functionality here that must run AFTER EVERY build.


#app-debug-enable: ## Enable Symfony Debug
#	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'export PROFILER_ENABLED=true'

#app-debug-disable: ## Disable Symfony Debug (Better performance)
#	${CMD_PREFIX} $(DOCKER_COMPOSE_BIN) $(COMPOSE_FILES_PATH) exec app /bin/sh -c 'export PROFILER_ENABLED=false'

php-cs-fixer-diff:
	@ echo "Run PHP codestyle fixer and compare to source..."
	@if $(DOCKER_BIN) run --rm \
		-v $(CURRENT_DIR):/data \
		cytopia/php-cs-fixer:latest \
		fix --dry-run --diff .; then \
		echo "OK"; \
	else \
		echo "Failed! Execute PHP code style fixer to fix this error."; \
		exit 1; \
	fi
