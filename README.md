
## Installation

#### Install Docker

Install docker if you do not have docker installed already.

##### Install Docker on Linux
Go to https://www.docker.com and follow the installation instructions.

Alternatively, a helper script is provided.
```bash
bin/get-docker.sh
```

Do not forget to add your user to the docker group.
**You may need to log out and back in for this to take effect!**
```
sudo usermod -aG docker `whoami`
newgrp docker
```


##### Install Docker on OSX
```text
Download and install the latest docker version:

https://www.docker.com/products/docker-desktop
```

#### Install Docker Compose

Install the latest version of docker-compose. Your version of docker-compose must at least support the compose schema version **3.7**.

```bash
DOCKER_COMPOSE_VERSION=1.27.4
sudo curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

*If you are working on OSX and you get weird python (http related, file not found) errors try to update or re-install docker. There are numerous similar issues to be found online.*
*This seems to be a docker-thing on OSX. The problem magically disappeared when I tried to upgrade Docker and cancelled due to insufficient user rights.*
**Weird!**

#### Install expect

Install expect to automate interactive CLI commands

##### Install expect on Linux (Debian based)
```
sudo apt-get install expect -y
```

##### Install expect on Linux (Red Hat / CentOS)
```
sudo yum install expect
```

##### Install expect on OSX
```
brew install expect
```


#### Install xclip
Install xclip to copy data to the clipboard. It is used to automatically copy the contents of the public key to the clipboard.

##### Install xclip on Linux (Debian based)
```
sudo apt-get install xclip -y
```

##### Install expect on Linux (Red Hat / CentOS)
```
sudo yum install xclip
```

##### Install expect on OSX
```
brew install xclip
```

## Get started

### Set your developer settings

Copy .developer.env.dist to .developer.env and set the correct values.

### Install your environment

Execute the install command
```bash
make install
```

Things to take into consideration:
- Currently, the containers are based on Alpine Linux. Which is smaller but take longer to build. I am planning on adding debian as well.
- When the development container is started for the first time, some scripts are executed. (composer install for example.). So it may take a while before the container becomes responsive. I am planning on adding an interactive install script in bash so further simplify the process.

