Execute reset command
```bash
make app-reset
```
```bash
> Execute `Frontend` commands y/n (default: false)?

# In this new docker setup yarn is not available in the container.
# Instead, to install frontend packages a separate container is started and removed when the command is finished.
# The frontend commands should not be executed.

Enter: n

> Execute `Postcode` commands y/n (default: false)?
Enter: y

> Enter country/countries to update, comma seperated (e.g.: "0,1,2,3")
  [0] NL
  [1] BE
  [2] LU
  [3] DE

Enter: 0,1

> Execute `Postcode` commands y/n (default: false)?
Enter: y

> Import `SQL Files` y/n (default: no)?
Enter: y

> Execute `Jobs` commands y/n (default: false)?
Enter: n

> Execute `Migrations` command instead of `Schema` command y/n (default: false)?
Enter: depends I guess, I picked 'y'
```


