#!/bin/bash
set -e

#
# Script variables
#

NON_INTERACTIVE=false

#
# Functions
#

read_env() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

#
# Validations
#

if ! [ -x "$(command -v git)" ]; then
  echo 'The program "git" is not installed or is not executable!' >&2
  exit 1
fi

MAKE_FILE="${PWD}/Makefile"
if [ ! -f "$MAKE_FILE" ]; then
    echo "$MAKE_FILE not found! Check your current working directory." >&2
    exit 1
fi

DEVELOPER_ENV_FILE="${PWD}/.developer.env"
if [ ! -f "$DEVELOPER_ENV_FILE" ]; then
    echo "$DEVELOPER_ENV_FILE not found! You may be using this script incorrectly. Execute 'make install' to proceed." >&2
    exit 1
fi

#
# Parse commandline arguments
#

while getopts ":hn" opt; do
  case ${opt} in
    h )
      echo "Usage:"
      echo "git-repository-webshop.sh [-n NON INTERACTIVE] [-h HELP]"
      exit 0
      ;;
    n )
      NON_INTERACTIVE=true
      ;;
   \? )
     echo "Invalid Option: -$OPTARG" 1>&2
     exit 1
     ;;
  esac
done
shift $((OPTIND -1))

#
# Set vars and check prerequisites (optionally prompt to run tasks if requirements not met)
#

DEVELOPER_WEBSITE_REPOSITORY=$(read_env DEVELOPER_WEBSITE_REPOSITORY .developer.env)
if [ -z "$DEVELOPER_WEBSITE_REPOSITORY" ]; then
   echo "\$DEVELOPER_WEBSITE_REPOSITORY is empty. Check your configuration in ${PWD}/.developer.env!" >&2
   exit 1
fi

DEVELOPER_SSH_RSA_ID=$(read_env DEVELOPER_SSH_RSA_ID .developer.env)
if [ -z "$DEVELOPER_SSH_RSA_ID" ]; then
   echo "\$DEVELOPER_SSH_RSA_ID is empty. Check your configuration in ${PWD}/.developer.env!" >&2
   exit 1
fi

SSH_KEY="$HOME/.ssh/$DEVELOPER_SSH_RSA_ID"
if [ ! -f "$SSH_KEY" ]; then

    if [ "$NON_INTERACTIVE" != "true" ]; then
        echo -n "Cannot find your SSH key. The '$SSH_KEY' file does not exist. Do you want to continue and generate your SSH key? (y/n) [y]: "
        read decision
        if [ "$decision" != "y" ]; then
            echo "SSH key is not available. Exiting..." >&2
            exit 1
        fi
    fi

    if ! sh -x "${PWD}/make ssh-key-gen"; then
        echo "Error occurred while generating the SSH key. Exiting..." >&2
        exit 1
    fi

    if ! sh -x "${PWD}/make ssh-key-add"; then
        echo "Error occurred while adding the SSH key to the SSH agent. Exiting..." >&2
        exit 1
    fi

    if [ ! -f "$SSH_KEY" ]; then
        echo "Error: SSH key should have been created but still cannot be found... The script may be broken." >&2
        exit 1
    fi
fi

#
# Main script
#

WORKSPACE_WEBSITE_DIR="${PWD}/workspace/webshop"
if [ -d "$WORKSPACE_WEBSITE_DIR" ]; then

    echo "The directory '$WORKSPACE_WEBSITE_DIR' already exists."

    if [ -d "$WORKSPACE_WEBSITE_DIR/.git" ]; then

        echo "Found .git folder in the website directory..."
        echo "Verifying git repository..."

        # Get remote
        GIT_REMOTE=$(git --git-dir $WORKSPACE_WEBSITE_DIR/.git remote -v)
        if [[ $GIT_REMOTE == *"$DEVELOPER_WEBSITE_REPOSITORY"* ]]; then
          echo "Validation passed!"
          echo "No changes have been made. The git repository already exists..."
          exit 0
        fi


        if [ "$NON_INTERACTIVE" == "true" ]; then
            echo "The $WORKSPACE_WEBSITE_DIR/.git directory was found but the upstream appears to be pointing to another repository." >&2
            exit 1
        fi

        echo "The $WORKSPACE_WEBSITE_DIR/.git directory was found but the upstream appears to be pointing to another repository."
        echo ""

        echo -n "Do you want to update the remote url? (y/n) [n]: "
        read decision
        if [ "$decision" == "y" ]; then
          git --git-dir "$WORKSPACE_WEBSITE_DIR/.git" remote set-url origin "$DEVELOPER_WEBSITE_REPOSITORY"
          echo "OK! The new repository remote was successfully changed to $DEVELOPER_WEBSITE_REPOSITORY"
          exit 0
        fi

        echo "Using the existing repository. Nothing left to do here. Exiting..."
        exit 0
    fi

    echo "The project directory exists but the folder $WORKSPACE_WEBSITE_DIR/.git was not found."
    echo "Either delete the project folder or manually initialize the git repository."
    echo ""
    echo "For example:"
    echo "git init && git remote add origin $DEVELOPER_WEBSITE_REPOSITORY"

else
    # Clone the repository
    mkdir -p "$WORKSPACE_WEBSITE_DIR"
    cd "$WORKSPACE_WEBSITE_DIR"
    echo ""
    echo "Cloning repository into $WORKSPACE_WEBSITE_DIR..."
    git clone "$DEVELOPER_WEBSITE_REPOSITORY" .
    if [ -d "$WORKSPACE_WEBSITE_DIR/.git" ]; then
        echo ""
        echo "OK! Successfully cloned repository into $WORKSPACE_WEBSITE_DIR..."
        if [ "$NON_INTERACTIVE" == "true" ]; then
            exit 0
        fi
    else
        echo ""
        echo "Failed to clone repository!" >&2
        exit 1
    fi
fi

if [ -d "$WORKSPACE_WEBSITE_DIR" ]; then
  OS_UNAME="`uname`"
  case $OS_UNAME in
    'Linux')
      # Check if xdg-open is available on Linux
      if [ -x "$(command -v xdg-open)" ]; then
          echo -n "Do you want to open the project folder? (y/n) [y]: "
          read decision
          if [ "$decision" == "y" ]; then
            xdg-open "$WORKSPACE_WEBSITE_DIR"
          fi
      fi
      ;;
    'Darwin')
      echo -n "Do you want to open the project folder? (y/n) [y]: "
      read decision
      if [ "$decision" == "y" ]; then
        open "$WORKSPACE_WEBSITE_DIR"
      fi
    ;;
    *) ;;
  esac

  exit 0
fi

echo ""
echo "Error: Something went wrong. Project folder '$WORKSPACE_WEBSITE_DIR' not found!" >&2
exit 1