#!/bin/bash

if ! [ -x "$(command -v expect)" ]; then
  echo 'The program "expect" is not installed or is not executable. Check the README.md file for instructions.' >&2
  exit 1
fi

#
# Execute reset command
#

COMMAND="$1 exec app /bin/sh -c \"$2\""

/usr/bin/expect -c "
    set timeout -1
    spawn ${COMMAND}
    expect \"*Frontend*\"
    send \"n\r\"
    expect \"*Postcode*\"
    send \"y\r\"
    expect \"*country/countries*\"
    send \"0,1\r\"
    expect \"*SQL*\"
    send \"y\r\"
    expect \"*Jobs*\"
    send \"n\r\"
    expect \"*Migrations*\"
    send \"y\r\"
    interact
"