#!/bin/bash
set -e

#
# Script variables
#

UPLOAD_SSH_KEY_URL="https://bitbucket.org/account/settings/ssh-keys/"
ENV_FILE=$PWD/.env
DEVELOPER_ENV_FILE=$PWD/.developer.env
NON_INTERACTIVE=false

#
# Functions
#

read_env() {
    VAR=$(grep $1 $2 | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

#
# Validations
#

MAKE_FILE=$PWD/Makefile
if [ ! -f "$MAKE_FILE" ]; then
    echo "$MAKE_FILE not found! Check your current working directory." 1>&2
    exit 1
fi

if ! [ -x "$(command -v ssh-keygen)" ]; then
  echo 'Program "ssh-keygen" not found or the program is not executable!' 1>&2
  exit 1
fi

#
# Parse commandline arguments
#

while getopts ":hn" opt; do
  case ${opt} in
    h )
      echo "Usage:"
      echo "ssh-key-gen.sh [-n NON INTERACTIVE] [-h HELP]"
      exit 0
      ;;
    n )
      NON_INTERACTIVE=true
      ;;
   \? )
     echo "Invalid Option: -$OPTARG" 1>&2
     exit 1
     ;;
  esac
done
shift $((OPTIND -1))

#
# Main script
#

if [ -f $ENV_FILE -a -f $DEVELOPER_ENV_FILE ]; then

  DEVELOPER_EMAIL=$(read_env DEVELOPER_EMAIL $DEVELOPER_ENV_FILE)
  DEVELOPER_SSH_RSA_ID=$(read_env DEVELOPER_SSH_RSA_ID $DEVELOPER_ENV_FILE)

  SSH_KEY="$HOME/.ssh/$DEVELOPER_SSH_RSA_ID"
  if [ -f "$SSH_KEY" ]; then
      if [ "$NON_INTERACTIVE" == "true" ]; then
        exit 0
      fi
      echo "SSH key $SSH_KEY already exists..."
  else

      if [ "$NON_INTERACTIVE" == "true" ]; then
        # Generate SSH key and exit...
        ssh-keygen -t rsa -b 4096 -C $DEVELOPER_EMAIL -f $SSH_KEY
        exit 0
      fi

      # Generate SSH key?
      echo "SSH key $SSH_KEY not found. Generating SSH key..."
      echo -n "Do you want to continue and generate your SSH key? [y]: "
      read decision
      if [ "$decision" != "y" ]; then
        echo "Exiting. No changes made."
        exit 1
      fi

      echo ""
      echo "Email address: $DEVELOPER_EMAIL"
      echo "SSH KEY: ~/.ssh/$DEVELOPER_SSH_RSA_ID"
      echo -n "Is this correct? (y/n) [n]: "
      read decision
      if [ "$decision" != "y" ]; then
        echo "Edit .developer.env and set the correct values. Execute this command again when you are ready."
        exit 1
      fi

      ssh-keygen -t rsa -b 4096 -C $DEVELOPER_EMAIL -f $SSH_KEY
  fi

  echo ""
  echo "Add your public key ($SSH_KEY.pub) to bitbucket: "
  echo "$UPLOAD_SSH_KEY_URL"

  # If the OS supports it, prompt user to open the webpage
  OS_UNAME="`uname`"
  case $OS_UNAME in
    'Linux')
      # Check if xdg-open is available on Linux
      if [ -x "$(command -v xdg-open)" ]; then
          echo -n "Do you want to open the webpage? (y/n) [n]: "
          read decision
          if [ "$decision" == "y" ]; then
            xdg-open "$UPLOAD_SSH_KEY_URL"
          fi
      fi
      ;;
    'Darwin')
      echo -n "Do you want to open the webpage? (y/n) [n]: "
      read decision
      if [ "$decision" == "y" ]; then
        open "$UPLOAD_SSH_KEY_URL"
      fi
    ;;
    *) ;;
  esac

  exit 0
fi

# If .developer.env.dist exists while .developer.env does not, there is a good chance that this is a new installation
# and the developer is not using the make file.
if [ -f "$DEVELOPER_ENV_FILE.dist" ]; then
   echo ""
   echo "Please do not execute this script directly. Run 'make help' to list the available build targets..."
   exit 1
fi

echo "Some required configuration files are missing. Your installation seems to be broken or incomplete! Exiting..." 1>&2
exit 1