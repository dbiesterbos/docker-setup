# ---------------------------------------------- Build Time Arguments --------------------------------------------------
ARG PHP_VERSION="7.3.9"
ARG NGINX_VERSION="1.17.4"
ARG COMPOSER_VERSION="1.10.17"
#ARG COMPOSER_VERSION="2.0.6"

ARG DOCKER_UID
ARG DOCKER_GID
ARG COMPOSER_AUTH={}

# -------------------------------------------------- Composer Image ----------------------------------------------------
FROM composer:${COMPOSER_VERSION} as composer

ARG DOCKER_UID
ARG DOCKER_GID
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_MEMORY_LIMIT -1

# Quicken Composer Installation by paralleizing downloads
ONBUILD RUN composer global require hirak/prestissimo --prefer-dist

# Copy Dependencies files
COPY workspace/webshop/composer.json composer.json
COPY workspace/webshop/composer.json composer.lock

# Set PHP Version of the Image
RUN composer config platform.php ${PHP_VERSION}

# A Json Object with Bitbucket or Github token to clone private Repos with composer
# Reference: https://getcomposer.org/doc/03-cli.md#composer-auth
ENV COMPOSER_AUTH $COMPOSER_AUTH

# ======================================================================================================================
#                                                   --- Base ---
# ---------------  This stage install needed extenstions, plugins and add all needed configurations  -------------------
# ======================================================================================================================

# There are -many- tags available. PHP consistently publishes tags in various formats.
# For example, 7.2.34-fpm-alpine3.12, 7.2-fpm-alpine, 7.2.34-fpm-alpine, 8.0-fpm, 8.0-cli, 8.0-cli-alpine etc.
# The "${PHP_VERSION}-fpm-alpine${ALPINE_VERSION}" works well for us. Note that $ALPINE_VERSION is optional.
# When omitted, the tag is still valid.
# See: https://hub.docker.com/_/php?tab=tags&page=1&name=fpm-alpine
FROM php:${PHP_VERSION}-fpm-alpine${ALPINE_VERSION} AS php-alpine

ARG DOCKER_UID
ARG DOCKER_GID
ENV DOCKER_UID $DOCKER_UID
ENV DOCKER_GID $DOCKER_GID

# ------------------------------------- Install Packages Needed Inside Base Image --------------------------------------

RUN echo "Installing dependencies..."

RUN apk add --no-cache		\
#    # -----  Needed for Image----------------
	fcgi tini shadow autoconf g++ libtool  make \
#    # -----  PHP Deps -----------------------
    bzip2-dev  \
    curl-dev \
    gettext-dev \
    icu-dev icu \
    rabbitmq-c-dev \
    libsodium libsodium-dev  \
    libssh-dev \
    libxml2-dev \
    libxslt-dev \
    libzip-dev \
#    # -----  Image Optimization --------------------
    libpng-dev  \
    libwebp-dev \
    zlib-dev \
    libxpm-dev \
    libjpeg-turbo-dev \
    optipng jpegoptim

RUN echo "Installing extensions..."

RUN docker-php-ext-install mbstring \
    	&& docker-php-ext-install pdo pdo_mysql \
    	&& docker-php-ext-install bcmath \
    	&& docker-php-ext-install curl \
    	&& docker-php-ext-install ctype \
    	&& docker-php-ext-install dom \
    	&& docker-php-ext-install json \
    	&& docker-php-ext-install iconv \
    	&& docker-php-ext-install gettext \
    	&& docker-php-ext-install exif \
    	&& docker-php-ext-install gd \
    	&& docker-php-ext-install bz2 \
    	&& docker-php-ext-install opcache \
    	&& docker-php-ext-install intl \
        && docker-php-ext-install sockets \
        && docker-php-ext-install sodium \
        && docker-php-ext-install xml \
        && docker-php-ext-install xmlrpc \
        && docker-php-ext-install xsl \
        && docker-php-ext-install soap \
        && docker-php-ext-install zip
        
# Imagick
RUN apk add --no-cache  imagemagick-dev pcre-dev && pecl install -o -f imagick && docker-php-ext-enable imagick

    # AMQP
RUN apk add --no-cache --update --virtual .phpize-deps $PHPIZE_DEPS \
    && pecl install -o -f amqp \
    && docker-php-ext-enable amqp

    # Redis
#ENV PHPREDIS_VERSION 5.1.1
ENV PHPREDIS_VERSION 4.0.2
RUN docker-php-source extract \
    && apk add --no-cache --virtual .phpize-deps-configure $PHPIZE_DEPS \
    && pecl install redis-${PHPREDIS_VERSION} \
    && docker-php-ext-enable redis


RUN echo "Dependencies installed..."

# ------------------------------------------------------ USER ----------------------------------------------------------

ONBUILD RUN groupmod -g ${DOCKER_GID} www-data
ONBUILD RUN usermod -g ${DOCKER_GID} -u ${DOCKER_UID} www-data

# ------------------------------------------------------ PHP -----------------------------------------------------------

COPY workbench/docker/conf/php/php-prod.ini  $PHP_INI_DIR/php.ini
COPY workbench/docker/conf/php/symfony.ini   $PHP_INI_DIR/conf.d/symfony.ini

# ---------------------------------------------------- Composer --------------------------------------------------------

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_MEMORY_LIMIT -1
ONBUILD COPY --from=composer /usr/bin/composer /usr/bin/composer

# ----------------------------------------------------- MISC -----------------------------------------------------------

WORKDIR /var/www/app
ENV APP_ENV prod
ENV APP_DEBUG 0

RUN chown ${DOCKER_UID}:${DOCKER_GID} /var/www/app -R

# Set perms and with setgid bit to recursively ensure correct owner
RUN chmod 2755 /var/www/app/
RUN find /var/www/app -type d -exec chmod 2755 {} \;

# -------------------------------------------------- ENTRYPOINT --------------------------------------------------------

# Add scripts and Entrypoint + clean!
COPY workbench/docker/healthcheck.sh			/usr/local/bin/docker-healthcheck
COPY workbench/docker/post-deployment.sh		/usr/local/bin/docker-post-deployment
COPY workbench/docker/bin/base/*		/usr/local/bin/
RUN  chmod +x /usr/local/bin/docker* && rm -rf /var/www/* /usr/local/etc/php-fpm.d/*

HEALTHCHECK CMD ["docker-healthcheck"]
CMD ["docker-base-entrypoint"]

# ======================================================================================================================
#                                                   --- FPM ---
# ---------------  This stage will install composer runtime dependinces and install app dependinces.  ------------------
# ======================================================================================================================

FROM php-alpine AS fpm-alpine

ARG DOCKER_UID
ARG DOCKER_GID
ARG DOCKER_GID

# Copy PHP-FPM config, scripts, and validate syntax.
COPY workbench/docker/conf/php-fpm/	/usr/local/etc/php-fpm.d/
COPY workbench/docker/bin/fpm/	/usr/local/bin/

# Chmod scripts, validate Syntax
RUN chmod +x /usr/local/bin/docker-fpm-* && php-fpm -t


HEALTHCHECK CMD ["docker-fpm-healthcheck"]
ENTRYPOINT ["docker-fpm-entrypoint"]

# ======================================================================================================================
#                                                  --- NGINX ---
# ---------------------------------------  Installs the Nginx Webserver  -----------------------------------------------
# ======================================================================================================================
FROM nginx:${NGINX_VERSION}-alpine AS nginx

ARG DOCKER_UID
ARG DOCKER_GID

# Required for big UID's on alpine
RUN apk add --no-cache	shadow

RUN /usr/sbin/groupmod -g ${DOCKER_GID} www-data
RUN /usr/sbin/useradd -l -s /bin/sh -g ${DOCKER_GID} -u ${DOCKER_UID} www-data

#RUN apk add --no-cache openssl	&& \
# 	openssl dhparam -out "/etc/nginx/dhparam.pem" 2048	&& \
# 	rm -rf /var/www/* /etc/nginx/conf.d/* /usr/local/etc/php-fpm.d/*

COPY workbench/docker/bin/nginx /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-nginx-*

# Copy Nginx Config
COPY workbench/docker/conf/nginx/ /etc/nginx/
COPY workbench/docker/conf/nginx/conf-available /etc/nginx/conf-available

RUN rm /etc/nginx/conf.d/default.conf

# Add Healthcheck
HEALTHCHECK CMD ["docker-nginx-healthcheck"]

# Add Entrypoint
ENTRYPOINT ["docker-nginx-entrypoint"]

# ======================================================================================================================
#                                             --- CRON & SUPERVISOR ---
# -------------------------------- This stage will install crontab and supervisor   ------------------------------------
# ======================================================================================================================
# ----------------------------------------------------- CRON -----------------------------------------------------------

FROM php-alpine AS cron

ARG DOCKER_UID
ARG DOCKER_GID
ENV DOCKER_UID $DOCKER_UID
ENV DOCKER_GID $DOCKER_GID
COPY workbench/docker/conf/crontab /etc/crontab
RUN crontab /etc/crontab

ENTRYPOINT ["docker-base-cron-entrypoint"]

# -------------------------------------------------- SUPERVISOR --------------------------------------------------------

FROM php-alpine AS supervisor
RUN apk add --no-cache supervisor
COPY /workbench/docker/conf/supervisor/ /etc/supervisor/
ENTRYPOINT ["docker-base-supervisor-entrypoint"]

# ======================================================================================================================
#                                                  --- Vendor ---
# ---------------  This stage will install composer runtime dependencies and install app dependencies.  ----------------
# ======================================================================================================================

FROM composer as composer-dev

# Install Dependencies
# Never run scriptsa or platform checks etc at this point. This is a build process, so many dependencies needed to run
# the application are most likely not in place.
ONBUILD RUN composer install -n --ignore-platform-reqs --no-plugins --no-scripts --no-autoloader --prefer-dist



FROM composer as composer-prod
COPY workspace/webshop/composer.lock composer.lock
# Install Dependencies
# Never run scriptsa or platform checks etc at this point. This is a build process, so many dependencies needed to run
# the application are most likely not in place.
ONBUILD RUN composer install -n --ignore-platform-reqs --no-plugins --no-scripts --no-autoloader --no-dev --prefer-dist



# ======================================================================================================================
# ==========================================  DEVELOPMENT FINAL STAGES  ================================================
#                                                    --- DEV ---
# ======================================================================================================================

# ------------------------------------------------------ FPM -----------------------------------------------------------
FROM fpm-alpine AS fpm-dev-alpine

ARG DOCKER_UID
ARG DOCKER_GID
ENV APP_ENV dev
ENV DOCKER_UID $DOCKER_UID
ENV DOCKER_GID $DOCKER_GID
ENV APP_DEBUG 1

# Install Composer runtime deps, and dev utilits
RUN apk add --no-cache git make openssh-client unzip zip curl nano htop sqlite
#RUN docker-php-ext-install pdo_sqlite

# Install MailHog\mhsendmail to intercept calls to sendmail()
RUN apk --no-cache add --virtual build-dependencies \
    go \
    git \
  && mkdir -p /root/gocode \
  && export GOPATH=/root/gocode \
  && go get github.com/mailhog/mhsendmail \
  && mv /root/gocode/bin/mhsendmail /usr/local/bin \
  && rm -rf /root/gocode \
  && apk del --purge build-dependencies

# Xdebug
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS	&& \
    pecl install xdebug										&& \
    docker-php-ext-enable xdebug							&& \
    apk del -f .build-deps

#COPY workbench/docker/conf/php/xdebug.ini /usr/local/etc/php/conf-available/docker-php-ext-xdebug.ini
COPY workbench/docker/conf/php/xdebug.ini /usr/local/etc/php/conf-available/docker-php-ext-xdebug.ini

# PHP dev config
COPY workbench/docker/conf/php/php-dev.ini  $PHP_INI_DIR/php.ini
COPY workbench/docker/conf/php/xdebug.ini  $PHP_INI_DIR/xdebug.ini
COPY workbench/docker/conf/php/msendmail.ini  $PHP_INI_DIR/msendmail.ini
COPY workbench/docker/conf/php-fpm/docker.conf.development	/usr/local/etc/php-fpm.d/docker.conf

# Entrypoint Scripts
COPY workbench/docker/bin/dev/	/usr/local/bin/
RUN  chmod +x /usr/local/bin/docker-dev-*
#RUN  chown ${DOCKER_UID}:${DOCKER_GID} /usr/local/bin/docker-dev-*


RUN apk del .phpize-deps-configure
RUN docker-php-source delete
RUN apk del --verbose .phpize-deps \
		autoconf \
		g++ \
		gcc \
		libbz2 \
		libgcc \
		libstdc++ \
		libtool \
		make \
		musl-dev \
		zlib-dev

RUN rm -rf /tmp/* /var/cache/apk/* /tmp/pear

# ------------------------------------------------------ USER ----------------------------------------------------------

RUN groupmod -g ${DOCKER_GID} www-data
RUN usermod -g ${DOCKER_GID} -u ${DOCKER_UID} www-data

# ------------------------------------------------------ PHP -----------------------------------------------------------

COPY workbench/docker/conf/php/php-dev.ini  $PHP_INI_DIR/php.ini
COPY workbench/docker/conf/php/symfony.ini   $PHP_INI_DIR/conf.d/symfony.ini

# ---------------------------------------------------- Composer --------------------------------------------------------

ENV COMPOSER_ALLOW_SUPERUSER 1
ONBUILD COPY --from=composer /usr/bin/composer /usr/bin/composer

# ----------------------------------------------------- MISC -----------------------------------------------------------

WORKDIR /var/www/app
ENV APP_ENV dev
ENV APP_DEBUG 1

RUN chown ${DOCKER_UID}:${DOCKER_GID} /var/www/app -R
RUN chmod 2755 /var/www/app/
RUN find /var/www/app -type d -exec chmod 2755 {} \;


ENTRYPOINT ["docker-dev-fpm-entrypoint"]

# For Runtime `composer install`
ENV COMPOSER_AUTH $COMPOSER_AUTH

# ----------------------------------------------------- NGINX ----------------------------------------------------------
FROM nginx AS nginx-dev
ENV APP_ENV dev

# Init SSL Certificates & Validate Conf Syntax
COPY workbench/docker/development/ssl/* "/etc/nginx/ssl/"
COPY workbench/docker/conf/nginx/dev.nginx.conf "/etc/nginx/nginx.conf"

COPY workbench/docker/conf/php/php-dev.ini  $PHP_INI_DIR/php.ini

# ======================================================================================================================
# ===========================================  PRODUCTION FINAL STAGES  ================================================
#                                                   --- PROD ---
# ======================================================================================================================

# ----------------------------------------------------- NGINX ----------------------------------------------------------
FROM nginx AS nginx-prod
COPY workspace/webshop/public /var/www/app/public
VOLUME ["/etc/nginx/ssl"]
EXPOSE 80 443

# ------------------------------------------------------ FPM -----------------------------------------------------------
FROM fpm-alpine AS fpm-prod-alpine
ONBUILD COPY --from=composer-prod /app/vendor /var/www/app/vendor
COPY workspace/webshop/* .

RUN apk del .phpize-deps-configure \
    && docker-php-source delete \
	&& apk del .phpize-deps \
		autoconf \
		bash \
		binutils \
		binutils-libs \
		db \
		expat \
		file \
		g++ \
		gcc \
		gdbm \
		gmp \
		isl \
		libatomic \
		libbz2 \
		libc-dev \
		libffi \
		libgcc \
		libgomp \
		libldap \
		libltdl \
		libmagic \
		libsasl \
		libstdc++ \
		libtool \
		m4 \
		make \
		mpc1 \
		mpfr3 \
		musl-dev \
		perl \
		pkgconf \
		pkgconfig \
		python \
		re2c \
		readline \
		zlib-dev \
	&& rm -rf /tmp/* /var/cache/apk/* /tmp/pear

RUN docker-base-prod-install

# ----------------------------------------------------- CRON -----------------------------------------------------------
FROM cron AS cron-prod
ONBUILD COPY --from=composer-prod /app/vendor /var/www/app/vendor
COPY workspace/webshop/* .
RUN docker-base-prod-install

# -------------------------------------------------- SUPERVISOR --------------------------------------------------------
FROM supervisor AS supervisor-prod
ONBUILD COPY --from=composer-prod /app/vendor /var/www/app/vendor
COPY workspace/webshop/* .
RUN docker-base-prod-install
